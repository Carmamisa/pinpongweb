<nav class="flex items-center justify-between flex-wrap bg-sky-700 p-6">
  <div class="flex items-center flex-shrink-0 text-sky-200 mr-6">
    <img class="fill-current  mr-2" width="35" height="50"  src="{{url('/images/logo.svg')}}"></img>
    <span class="font-semibold text-xl tracking-tight">CTM Mos</span>
  </div>
  <div class="block md:hidden">
    <button id="botonMenu" class="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
      <svg class="fill-current h-3 w-3 cursor-pointer" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </button>
  </div>
  <div class="w-full block flex-grow md:flex md:items-center md:w-auto md:ml-10 ">
    <div id="menu" class="hidden md:block  md:ex-grow">
      <x-nav-link :href="route('publicaciones')" :active="request()->routeIs('publicaciones')" class="block mt-4 md:inline-block md:mt-0 text-teal-200 hover:text-white md:mr-6 text-xl">
        Publicaciones
      </x-nav-link>
      <x-nav-link :href="route('equipos')" :active="request()->routeIs('equipos')" class="block mt-4 md:inline-block md:mt-0 text-teal-200 hover:text-white md:mr-6 text-xl">
        Gestion de equipos
      </x-nav-link>
      <x-nav-link :href="route('resultados')" :active="request()->routeIs('resultados')" class="block mt-4 md:inline-block md:mt-0 text-teal-200 hover:text-white text-xl">
        Resultados
      </x-nav-link>
    </div>
    
  </div>
</nav>
<script>
    document.getElementById("botonMenu").addEventListener('click',function(){
        var element= document.getElementById('menu');
        if(element.classList.contains('block')){
            element.classList.remove('block');
            element.classList.add('hidden');
        }else{
            element.classList.remove('hidden');
            element.classList.add('block');
        }
    })
</script>