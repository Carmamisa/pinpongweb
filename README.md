# Aplicación web para gestión de clubes de Ping Pong

Existen tres roles para el uso de esta aplicación.

- **Aficionado sin registrar:** puede realizar las siguientes visualizaciones públicas:
  - Noticias del club
  - Imágenes por categorías
  - Clasificaciones
- **Aficionado registrado**: además de las mismas visualizaciones que el aficionado sin registrar, realizar visualizaciones exclusivas para el club.
- **Administrador**: gestiona lo siguiente:
  - Usuarios (aceptar o eliminar)
  - Noticias del club (crear, editar, eliminar)
  - Categorías (crear, editar, eliminar)
  - Imágenes por categorías (crear, editar, eliminar)
  - Temporadas (crear, editar, eliminar)
    - Jugadores (crear, editar, eliminar)
    - Jornadas (crear, editar, eliminar)