<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/publicaciones', [PostController::class,'index'])->name('publicaciones');
    Route::get('/equipos', function () {
        return view('administrador/equipos');
    })->name('equipos');
    Route::get('/resultados', function () {
        return view('administrador/resultados');
    })->name('resultados');
});
